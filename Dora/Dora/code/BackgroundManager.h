//
//  BackgroundManager.h
//  Dora
//
//  Created by Jason Chang on 8/9/12.
//
//

#import "Manager.h"

@interface BackgroundManager : Manager
{
    CCSprite* _backBG;
    CCSprite* _frontBG;
}

@property (readonly) CGRect stageBoundary;
@property (readonly) float groundHeight;

@end
