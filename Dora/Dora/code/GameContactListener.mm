//
//  GameContactListener.m
//  Dora
//
//  Created by  on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameContactListener.h"

#define SMALLNUMBER 0.005f

GameContactListener::GameContactListener()
{
    
}

GameContactListener::~GameContactListener()
{
    
}

void GameContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{

}

void GameContactListener::handleCollision(b2Fixture* sensor, b2Fixture* target)
{

}

void GameContactListener::BeginContact(b2Contact* contact)
{

}

void GameContactListener::EndContact(b2Contact* contact)
{
    
    
}