//
//  EnemyManager.h
//  Dora
//
//  Created by Jason Chang on 8/9/12.
//
//

#import "Manager.h"
#import "EnemyContextDelegate.h"

@interface EnemyManager : Manager
{
    NSMutableArray* _spawnNpcTypeList;
    NSArray* _enemies;
    
    int _npcOnScreen;
    BOOL  _isWaitingBetweenWaves;
    float _passedTimeOfWaiting;
    int _currentWave;
    
    float _spawnInterval;
    float _spawnTimer;
}

@property (assign) CGRect stageBoundry;
@property (assign) float groundPosition;

@property (assign) id<EnemyContextDelegate> contextDelegate;
@property (readonly) int currentWave;
@property (readonly) int killCount;
@property (readonly) NSArray* enemyArray;

- (void) prepareSpawnEnemies;
- (void) spawnEnemy: (ccTime) dt;

// wave control related
- (void) generateSpawnNpcTypeList;

@end
