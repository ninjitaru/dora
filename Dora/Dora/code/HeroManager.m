//
//  HeroManager.m
//  Dora
//
//  Created by Jason Chang on 8/9/12.
//
//

#import "HeroManager.h"
#import "SideKick.h"
#import "SimpleAudioEngine.h"

@implementation HeroManager

@synthesize isHeroDead = _isHeroDead;

- (float) hpRatio
{
    return _hero.hp/(float)_maxHp;
}

- (CCSprite*) heroSprite
{
    return _hero.sprite;
}

- (void) spawnHero
{
    if(!_hero)
    {
        _hero = [[Hero alloc] initWithParent: _parent];
    }
    
    _maxHp = 1000;
    _originalDefense = 900;
    _initSpeed = 120;
    _originalPower = 100;
    
    float scale = 1.0f;
    if([CCDirector sharedDirector].contentScaleFactor == 1)
        scale = 0.5f;
    
    _hero.sprite.scale = scale;
    [self reset];
}

- (void) spawnSideKick
{
    [[SimpleAudioEngine sharedEngine] preloadEffect: @"dora_catch.wav"];
    [[SimpleAudioEngine sharedEngine] preloadEffect: @"dora_throw.wav"];
    _maxSideKickHp = 300;
    
    _npcs = [@[[[SideKick alloc] initWithParent: _parent],[[SideKick alloc] initWithParent: _parent],[[SideKick alloc] initWithParent: _parent]] retain];
    
    [self resetSideKick];
}

- (void) update:(ccTime)dt
{
    if(_hero.hp <= 0)
    {
        _hero.state = stateHeroDead;
        _isHeroDead = YES;
        return;
    }
    
    BOOL walkLeft = [self.controlDelegate isButtonDown: ButtonLeft];
    BOOL walkRight = [self.controlDelegate isButtonDown: ButtonRight];
    
    float direction = -1.0f;
    if(walkLeft && walkRight)
    {
        _hero.state = stateHeroIdle;
    }
    else if(walkLeft)
    {
        _hero.direction = kDirectionLeft;
        _hero.state = stateHeroWalk;
    }
    else if(walkRight)
    {
        _hero.direction = kDirectionRight;
        _hero.state = stateHeroWalk;
        direction = 1.0f;
    }
    else
    {
        _hero.state = stateHeroIdle;
    }
    
    if([self.controlDelegate isButtonDown: ButtonMelee])
    {
        [self performMelee];
        
    }
    else if([self.controlDelegate isButtonDown: ButtonRange])
    {
        [self performRange];
        // TODO spawn bullet
    }
    
    if([self.controlDelegate isButtonDown: ButtonGrab] && !_grabDown)
    {
        _grabDown = YES;
    }
    if([self.controlDelegate isButtonUp: ButtonGrab] && _grabDown)
    {
        _grabDown = NO;
        _grabClicked = YES;
    }
    
    if(_grabClicked)
    {
        _grabClicked = NO;
        [self grabSideKick];
    }
    
    if(_hero.state == stateHeroWalk)
    {
        float displacement = _heroSpeed * direction * dt;
        float x = _hero.sprite.position.x + displacement;
        x = x < _heroPositionLimit.x ? _heroPositionLimit.x : x;
        x = x > _heroPositionLimit.y ? _heroPositionLimit.y : x;
        CGPoint pos = _hero.sprite.position;
        pos.x = x;
        _hero.sprite.position = pos;
    }

    for(SideKick* sidekick in _npcs)
    {
        [sidekick update: dt];
        if(sidekick.mode == kEquiped && !sidekick.isFullHp)
            _hero.hp -= 1;
        if(sidekick.mode == kDead && !sidekick.hurtHero)
        {
            sidekick.hurtHero = YES;
            [self adjustHeroDefenseByAdjustMaxDefense: YES];
        }
    }
    
    // TODO update bullet
}

- (void) adjustHeroDefenseByAdjustMaxDefense: (BOOL) should
{
    int count = 0;
    for(SideKick* sidekick in _npcs)
    {
        if(sidekick.mode != kDead)
            count++;
    }
    if(should)
    {
        _currentMaxDefense -= _originalDefense/3;
    }
    _hero.defense = _currentMaxDefense - (_currentMaxDefense/count * _equipedCount);
    //CCLOG(@"%i", _hero.defense);
    
}

- (void) performMelee
{
    if(_equipedCount >= 1)
    {
        for(SideKick* s in _npcs)
        {
            if(s.mode == kEquiped && s.equipSlot == 2)
            {
                [self releaseSideKick: 2];
                return;
            }
        }
    }
    _hero.state = stateHeroMelee;
}

- (void) performRange
{
    if(_equipedCount >= 1)
    {
        for(SideKick* s in _npcs)
        {
            if(s.mode == kEquiped && s.equipSlot == 1)
            {
                [self releaseSideKick: 1];
                return;
            }
        }
    }
    _hero.state = stateHeroRange;
}

- (void) reset
{
    _grabClicked = NO;
    _grabDown = NO;
    
    _hero.hp = _maxHp;
    _hero.defense = _originalDefense;
    _hero.power = _originalPower;
    _heroSpeed = _initSpeed;
    _currentMaxDefense = _originalDefense;
    _isHeroDead = NO;
    _hero.sprite.position = ccp(self.stageBoundry.size.width/2, self.groundPosition);
    _hero.state = stateHeroIdle;
    _hero.direction = kDirectionRight;
    _heroPositionLimit.x = _hero.sprite.boundingBox.size.width/2 * _hero.sprite.anchorPoint.x;
    _heroPositionLimit.y = self.stageBoundry.size.width - _hero.sprite.boundingBox.size.width/2 * _hero.sprite.anchorPoint.x;
    
    [self resetSideKick];
}

- (void) resetSideKick
{
    _equipedCount = 0;
    for(SideKick* sidekick in _npcs)
    {
        float halfsize = sidekick.sprite.boundingBox.size.width/2;
        CGPoint pos = ccp(CCRANDOM_0_1() * (self.stageBoundry.size.width-halfsize*3) + halfsize, 25);
        sidekick.sprite.position = pos;
        sidekick.maxHp = _maxSideKickHp;
        sidekick.hp = _maxSideKickHp;
        sidekick.mode = kFree;
        sidekick.equipSlot = 0;
        sidekick.hurtHero = NO;
        sidekick.groundHeight = self.groundPosition;
        sidekick.avoidTarget = _hero.sprite;
    }
}

- (void) detachAllSideKicks
{
    for(SideKick* sidekick in _npcs)
    {
        if(sidekick.mode == kEquiped)
        {
            sidekick.mode = kFree;
            sidekick.equipSlot = 0;
        }
    }
    _equipedCount = 0;
    [self adjustHeroDefenseByAdjustMaxDefense: NO];
}

#define MAXPUNCHDMG 1
#define MELEEATTACKRANGE 80.0f

- (void) tryKillEnemies: (NSArray*) enemies
{
    if(_hero.state == stateHeroMelee && _hero.carryMeleeDmg)
    {
        _hero.carryMeleeDmg = NO;
        int dmgCount = 0;
        //CGRect detectionRegion = _hero.sprite.boundingBox;
        
        for(Entity* e in enemies)
        {
            if(e.isValid && [_hero distanceTo: e] < MELEEATTACKRANGE)//CGRectIntersectsRect(detectionRegion, e.sprite.boundingBox))
            {
                dmgCount++;
                [e receiveHit: _hero.power];
            }
            if(dmgCount >= MAXPUNCHDMG)
                break;
        }
    }
    // TODO loop through bullet to kill enemy
}

#define GRABDISTANCE 30

- (void) grabSideKick
{
    for(SideKick* s in _npcs)
    {
        if(s.mode == kEquiped && s.equipSlot == 3)
        {
            [self releaseSideKick: 3];
            return;
        }
    }
    
    int index = 0;
    int targetIndex = -1;
    int minDistance = 2000;
    for(SideKick* s in _npcs)
    {
        if(s.mode == kFree && [_hero distanceTo: s] <= GRABDISTANCE)
        {
            int dist =  abs(_hero.sprite.position.x - s.sprite.position.x);
            if(dist < minDistance)
            {
                minDistance = dist;
                targetIndex = index;
            }
        }
        index++;
    }
    
    if(targetIndex != -1)
    {
        SideKick* s = [_npcs objectAtIndex: targetIndex];
        [[SimpleAudioEngine sharedEngine] playEffect: @"dora_catch.wav"];
        if(_equipedCount == 0)
        {
            s.equipSlot = 1;
            [s toEquiped: ccp(-50, 70)];
            
        }
        else if(_equipedCount == 1)
        {
            s.equipSlot = 2;
            [s toEquiped: ccp(0, 230)];
            
        }
        else
        {
            s.equipSlot = 3;
            [s toEquiped: ccp(65, 85)];
            
        }
        _equipedCount++;
    }
    
    if(_equipedCount == 3)
    {
        _heroSpeed = _initSpeed/2;
    }
    [self adjustHeroDefenseByAdjustMaxDefense: NO];
}

- (void) releaseSideKick: (int) slot
{
    for(SideKick* s in _npcs)
    {
        if(s.equipSlot == slot)
        {
            if(_equipedCount == 3)
            {
                _heroSpeed = _initSpeed;
            }
            [[SimpleAudioEngine sharedEngine] playEffect: @"dora_throw.wav"];
            float direction = 1;
            if(_hero.direction == kDirectionLeft)
                direction = -1;
            [s freeSideKick: direction];
            _equipedCount--;
            
            [self adjustHeroDefenseByAdjustMaxDefense: NO];
            return;
        }
    }
}

- (Entity*)findNewTarget:(float)position
{
    float lowestDist = abs(position - _hero.sprite.position.x);
    int index = 0;
    int useIndex = -1;
    for(SideKick* s in _npcs)
    {
        if(s.mode != kFree)
        {
            index++;
            continue;
        }
        float low = abs(position - s.sprite.position.x);
        if(low < lowestDist)
        {
            useIndex = index;
            lowestDist = low;
        }
        index++;
    }
    if(useIndex != -1)
    {
        return [_npcs objectAtIndex: useIndex];
    }
    return _hero;
}

- (void) dealloc
{
    [_npcs release];
    [_hero release];
    [super dealloc];
}

@end
