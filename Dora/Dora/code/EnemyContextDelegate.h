//
//  EnemyContextDelegate.h
//  Dora
//
//  Created by Jason Chang on 8/11/12.
//
//

#ifndef Dora_EnemyContextDelegate_h
#define Dora_EnemyContextDelegate_h

@class Entity;

@protocol EnemyContextDelegate <NSObject>

- (Entity*)findNewTarget:(float)position;

@end

#endif
