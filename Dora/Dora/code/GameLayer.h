//
//  GameLayer.h
//  Dora
//
//  Created by  on 4/28/12. Modified by Jason Chang 12/8/12
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CCLayer.h"
//#import "PhysicsManager.h"
#import "BackgroundManager.h"
#import "EnemyManager.h"
//#import "SideKickManager.h"
#import "HeroManager.h"

@class UILayer;

@interface GameLayer : CCLayer
{
    //PhysicsManager* _physicManager;
    BackgroundManager* _backgroundManager;
    EnemyManager* _enemyManager;
    //SideKickManager* _sidekickManager;
    HeroManager* _heroManager;
    
    CCSpriteBatchNode* _batchNode;
    
    float _timeAccum; // track total game time
    
    BOOL _gameOver;
    
    BOOL _retryDown;
    BOOL _retryClicked;
}

@property (nonatomic, assign) UILayer* uiDelegate;

+(CCScene *) scene;

@end
