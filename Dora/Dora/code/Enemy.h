//
//  Enemy.h
//  Dora
//
//  Created by Yuxio Tzeng on 12/4/28.
//  Copyright (c) 2012年 Qiran Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "Entity.h"
#import "GameTypes.h"
#import "EnemyContextDelegate.h"

typedef enum
{
    eTypeA = 0,
    eTypeB = 1,
} enumEnemyType;

@interface Enemy : Entity
{
    NSDictionary* _animationDict;
    id<EnemyContextDelegate> _delegate;
    
    float _speedRatio;
    Entity* _target;
    int _hp;
    float _delay;
    float _dmgDistance;
}

//+ (void)initialize;

@property (assign, nonatomic) enumEnemyState state;
@property (readonly) enumEnemyType enemyType;
@property (assign) int power;
@property (assign) BOOL recentlyDead;

- (id) initWithParent:(CCNode*)parent withContext:(id<EnemyContextDelegate>)context;

- (void) spawnAs: (enumEnemyType) type;
- (void) update: (ccTime) dt;


@end