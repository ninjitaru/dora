//
//  GameContactListener.h
//  Dora
//
//  Created by  on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Box2D.h"

class GameContactListener : public b2ContactListener
{
    
public:
    GameContactListener();
    virtual ~GameContactListener();
    
    virtual void BeginContact(b2Contact* contact);
    virtual void EndContact(b2Contact* contact);
    virtual void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
    virtual void PostSolve(const b2Contact* contact, const b2ContactImpulse* impulse)
    {
        B2_NOT_USED(contact);
        B2_NOT_USED(impulse);
    }
    
protected:
    
    void handleCollision(b2Fixture* sensor, b2Fixture* target);
    
};


