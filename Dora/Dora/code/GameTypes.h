//
//  GameTypes.h
//  Dora
//
//  Created by Jason Chang on 8/9/12.
//
//

#ifndef Dora_GameTypes_h
#define Dora_GameTypes_h

typedef enum
{
    ButtonPause,
    ButtonMelee,
    ButtonRange,
    ButtonGrab,
    ButtonLeft,
    ButtonRight,
    ButtonRetry,
} enumUIButton;

typedef enum
{
    phaseDown,
    phaseUp,
} enumButtonPhase;

typedef enum
{
    kDirectionNone,
    kDirectionLeft,
    kDirectionRight,
    kDirectionMax
} enumMoveDirection;

typedef enum
{
    stateNpcNone,
    stateNpcWalk,
    stateNpcAttack,
    stateNpcDead,
} enumEnemyState;

typedef enum
{
    stateHeroNone,
    stateHeroIdle,
    stateHeroWalk,
    stateHeroMelee,
    stateHeroMeleeStop,
    stateHeroRange,
    stateHeroRangeStop,
    stateHeroDead,
} enumHeroState;

typedef enum
{
    spriteDepthBackBG,
    spriteDepthFrontBG,
    spriteDepthGameObject,
    spriteDepthHero,
    spriteDepthEnemyBack,
    spriteDepthEnemyFront,
    spriteDepthFG,
    spriteDepthUI,
} enumSpriteDepth;

@protocol ControlDelegate <NSObject>

- (BOOL) isButtonDown: (enumUIButton) type;
- (BOOL) isButtonUp: (enumUIButton) type;

@end

#endif
