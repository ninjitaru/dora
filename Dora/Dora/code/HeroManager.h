//
//  HeroManager.h
//  Dora
//
//  Created by Jason Chang on 8/9/12.
//
//

#import "Manager.h"
#import "Hero.h"
#import "GameTypes.h"
#import "EnemyContextDelegate.h"

@interface HeroManager : Manager <EnemyContextDelegate>
{
    Hero* _hero;
    CGPoint _heroPositionLimit;
    
    float _heroSpeed;
    int _maxHp;
    int _originalDefense;
    float _initSpeed;
    int _currentMaxDefense;
    int _originalPower;
    
    NSArray* _npcs;
    int _maxSideKickHp;
    int _equipedCount;
    
    BOOL _grabDown;
    BOOL _grabClicked;
}

@property (assign) CGRect stageBoundry;
@property (assign) float groundPosition;
@property (readonly) CCSprite* heroSprite;
@property (assign) id<ControlDelegate> controlDelegate;
@property (readonly) BOOL isHeroDead;
@property (readonly) float hpRatio;

- (void) spawnHero;
- (void) spawnSideKick;
- (void) detachAllSideKicks;
- (void) tryKillEnemies: (NSArray*) enemies;


@end
