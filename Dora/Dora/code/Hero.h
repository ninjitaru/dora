//
//  Hero.h
//  Dora
//
//  Created by Jason Chang on 8/9/12.
//
//

#import "Entity.h"
#import "GameTypes.h"

@interface Hero : Entity
{
    CCSpriteFrameCache* _cache;
    NSDictionary* _animationDictionary;
}

@property (assign, nonatomic) enumHeroState state;
@property (assign, nonatomic) enumMoveDirection direction;
@property (assign, nonatomic) int hp;
@property (assign, nonatomic) int defense;
@property (assign, nonatomic) int power;
@property (assign) BOOL carryMeleeDmg;

- (id)initWithParent: (CCNode*) parent;

@end


