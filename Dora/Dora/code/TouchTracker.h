//
//  TouchTracker.h
//  DancingFinger
//
//  Created by  on 11/23/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TouchTracker : NSObject
{
    UITouch** trackedTouches;
    Byte maxTouches;
    uint touchCount;
}

@property (nonatomic, assign) uint touchCount;

- (UITouch*) getTouchByID: (int) tID;
- (int) getTouchID: (UITouch*) touch;
- (int) trackTouch: (UITouch*) touch;
- (void) releaseTouchByID: (int) tID;
- (void) releaseTouchByObject: (UITouch*) touch;
- (void) traverseTouchesWithTarget: (id) target Selector: (SEL) selector;
- (void) clear;

@end
