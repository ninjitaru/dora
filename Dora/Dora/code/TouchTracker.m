//
//  TouchTracker.m
//  DancingFinger
//
//  Created by  on 11/23/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "TouchTracker.h"

#define MAXFINGERCOUNT 4

@implementation TouchTracker

@synthesize touchCount;

- (id) init
{
    if((self = [super init]))
    {
        maxTouches = MAXFINGERCOUNT;
        trackedTouches = (UITouch**)malloc(sizeof(UITouch*) * maxTouches);
        for(int i = 0; i < maxTouches; i++)
            trackedTouches[i] = NULL;
        touchCount = 0;
    }
    return self;
}

- (int) getTouchID: (UITouch*) touch
{
    for(int i = 0; i < maxTouches; i++)
    {
        UITouch* storedTouch = trackedTouches[i];
        if(storedTouch == touch)
            return i;
    }
    return -1;
}

- (void) clear
{
    for(int i = 0; i < maxTouches; i++)
    {
        trackedTouches[i] = nil;
    }
}

- (int) trackTouch: (UITouch*) touch
{
    for(int i = 0; i < maxTouches; i++)
    {
        if(!trackedTouches[i])
        {
            touchCount++;
            trackedTouches[i] = touch;
            return i;
        }
    }
    
    return -1;
}

- (void) releaseTouchByID: (int) tID
{
    if(tID < 0 || tID >= maxTouches)
        return;
    
    trackedTouches[tID] = nil;
    touchCount--;
}

- (void) releaseTouchByObject: (UITouch*) touch
{
    for(int i = 0; i < maxTouches; i++)
    {
        UITouch* storedTouch = trackedTouches[i];
        if(storedTouch == touch)
        {
            trackedTouches[i] = nil;
            touchCount--;
            return;
        }
    }
}

- (void) traverseTouchesWithTarget: (id) target Selector: (SEL) selector
{
    for(int i = 0; i < maxTouches; i++)
    {
        if(trackedTouches[i])
            [target performSelector: selector withObject: [NSNumber numberWithInt: i]];
    }
}

- (UITouch*) getTouchByID: (int) tID
{
    if(tID >= 0 && tID < maxTouches)
        return trackedTouches[tID];
    return nil;
}

- (void) dealloc
{
    free(trackedTouches);
    [super dealloc];
}

@end
