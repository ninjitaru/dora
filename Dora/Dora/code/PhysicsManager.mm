//
//  PhysicsManager.m
//  Dora
//
//  Created by  on 12/28/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "PhysicsManager.h"
#import "GameContactListener.h"
#import "PhysicConstants.h"

#define CCPTOB2(a) ccpMult(a, _inversePTM)
#define CCPTOVEC(a) b2Vec2(a.x, a.y)

@implementation PhysicsManager

@synthesize inversePTM = _inversePTM;

#pragma mark - object setup

- (id) init
{
    if((self = [super init]))
    {
        _inversePTM = 1.0f/PTM_RATIO;
        [self reset];
    }
    return self;
}

- (void) reset
{
 
}

- (void) draw
{
    
}

- (void) dealloc
{

    [super dealloc];
}

#pragma mark - methods


#pragma mark - updates

- (void) update: (ccTime)dt
{

}

@end
