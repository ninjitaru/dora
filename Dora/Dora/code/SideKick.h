//
//  SideKick.h
//  Dora
//
//  Created by  on 4/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Entity.h"

typedef enum
{
    kFree,
    kEquiped,
    kInAir,
    kDead,
} SideKickMode;

@interface SideKick : Entity
{
    CCSpriteFrameCache* _cache;
    BOOL doMovement;
    float halfSize;
    float movementTimer;
    float _movementTime;
    float speed;
    int _spriteMode;
    CGPoint diffPos;
}

@property (nonatomic, assign) BOOL hurtHero;
@property (nonatomic, assign) int equipSlot;
@property (nonatomic, assign) SideKickMode mode;
@property (nonatomic, assign) int hp;
@property (assign, nonatomic) float maxHp;
@property (nonatomic, assign) CCSprite* avoidTarget;
@property (readonly) BOOL isFullHp;
@property (assign) float groundHeight;

- (id) initWithParent: (CCNode*) node;

- (void) update: (ccTime) dt;
//- (id) initWithParent: (CCNode*) node name: (NSString*) string;
//- (void) adaptPositionByMovement: (float)movement;
- (void) toEquiped: (CGPoint) pos;
- (void) freeSideKick: (float) direction;

@end
