//
//  Manager.h
//  Dora
//
//  Created by Jason Chang on 8/9/12.
//
//

#import "cocos2d.h"

@interface Manager : NSObject
{
    CCNode* _parent;
}

- (id) initWithParentNode: (CCNode*) node;
- (void) reset;
- (void) update: (ccTime) dt;

@end
