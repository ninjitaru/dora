//
//  SideKick.m
//  Dora
//
//  Created by  on 4/29/12. Modified by Jason Chang 12/8/12
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SideKick.h"
#import "GameTypes.h"

@implementation SideKick

@synthesize avoidTarget = _avoidTarget;

@synthesize hp = _hp;
@synthesize maxHp = _maxHp;

@synthesize mode = _mode;
@synthesize equipSlot = _equipSlot;
@synthesize hurtHero = _hurtHero;

#define SIDEKICKDEPTH 10
#define ACTIONRATE (1/30.0f)
#define SIDEKICKMAXHP 200.0f
#define RECOVERRATE (1/3.0f)

- (void) setHp:(int)hp
{
    _hp = hp;
    if(_hp < 0)
        _hp = 0;
    if(_hp > _maxHp)
        _hp = _maxHp;
}

- (BOOL) isFullHp
{
    return self.hp == self.maxHp;
}

- (id) initWithParent: (CCNode*) node
{
    if((self = [super init]))
    {
        _cache = [CCSpriteFrameCache sharedSpriteFrameCache];
        self.sprite = [CCSprite spriteWithSpriteFrameName: @"Cleaner80.png"];
        self.sprite.anchorPoint = ccp(0.5f, 0);
        [node addChild: self.sprite z: spriteDepthEnemyFront];
        _spriteMode = 1;
        _movementTime = 1;
        halfSize = self.sprite.boundingBox.size.width/2;
    }
    return self;
}

- (void) setMode:(SideKickMode)mode
{
    if(_mode == mode)
        return;
    switch(mode)
    {
        case kFree:
            [self alterSprite];
            break;
        case kDead:
            [self.sprite setDisplayFrame: [_cache spriteFrameByName: @"CleanerDie.png"]];
            _spriteMode = 4;
            break;
        case kEquiped:
            if(self.equipSlot == 1)
            {
                self.sprite.rotation = 45;
            }
            else if(self.equipSlot == 2)
            {
                self.sprite.rotation = 180;
            }
            else if(self.equipSlot == 3)
            {
                self.sprite.rotation = 315;
            }
            if(self.sprite.zOrder != spriteDepthGameObject)
                [self.sprite.parent reorderChild: self.sprite z: spriteDepthGameObject];
            break;
        case kInAir:
            break;
    }
    _mode = mode;
}

- (void) alterSprite
{
    float ratio = self.hp/self.maxHp;
    if(ratio <= 0.2f)
    {
        if(_spriteMode != 3)
        {
            _spriteMode = 3;
            [self.sprite setDisplayFrame: [_cache spriteFrameByName: @"Cleaner20.png"]];
        }
    }
    else if(ratio <= 0.8f)
    {
        if(_spriteMode != 2)
        {
            _spriteMode = 2;
            [self.sprite setDisplayFrame: [_cache spriteFrameByName: @"Cleaner50.png"]];
        }
    }
    else
    {
        if(_spriteMode != 1)
        {
            _spriteMode = 1;
            [self.sprite setDisplayFrame: [_cache spriteFrameByName: @"Cleaner80.png"]];
        }
    }
}

- (void) update: (ccTime) dt
{
    if(self.hp <= 0 && _mode != kDead)
    {
        self.hp = 0;
        self.mode = kDead;
        return;
    }
    
    if(_mode == kDead)
        return;
    
    [self alterSprite];
    
    if(_mode == kFree && self.avoidTarget)
    {
        if(self.sprite.zOrder != spriteDepthEnemyFront)
        {
            self.sprite.rotation = 0;
            [self.sprite.parent reorderChild: self.sprite z: spriteDepthEnemyFront];
        }
        
        movementTimer += dt;
        if(CCRANDOM_0_1() < ACTIONRATE)
        {
            doMovement = YES;
            movementTimer = 0;
            _movementTime = CCRANDOM_0_1() * 3;
            speed = 60;
            if(CCRANDOM_0_1() > 0.5)
                speed *= -1;
        }
        
        if(doMovement)
        {
            if(movementTimer <= _movementTime)
            {
                CGPoint newPos = ccpAdd(self.sprite.position, ccp(speed*dt,0));
                if(newPos.x >= halfSize && newPos.x <= (960-halfSize))
                    self.sprite.position = newPos;
            }
            else
            {
                doMovement = NO;
            }
        }
    }
    
    if(_mode == kEquiped)
    {
        self.hp += 1;
        self.sprite.position = ccpAdd(self.avoidTarget.position, diffPos);
    }
    
    if(_mode == kInAir)
    {
        CGPoint pos = self.sprite.position;
        if(pos.y > self.groundHeight)
        {
            pos.y -= 300*dt;
            if(pos.y <= self.groundHeight)
            {
                self.mode = kFree;
                pos.y = self.groundHeight;
            }
            float x = pos.x + 240*diffPos.x*dt;
            if(x >= halfSize && x <= (960-halfSize))
                pos.x = x;
        }
        self.sprite.position = pos;
    }
}

- (void) freeSideKick: (float) direction
{
    self.equipSlot = 0;
    self.mode = kInAir;
    diffPos.x = direction;
}

- (void) toEquiped: (CGPoint) pos;
{
    diffPos = pos;
    self.mode = kEquiped;
}

//- (void) adaptPositionByMovement: (float)movement
//{
//    if(_mode == kEquiped)
//        return;
//    _sprite.position = ccp(_sprite.position.x - movement, _sprite.position.y); 
//}

//- (float) positionX
//{
//    return self.sprite.position.x;
//}

- (BOOL) isValid
{
    return (_mode == kFree);
}

- (void) receiveHit:(int)dmgPower
{
    self.hp -= dmgPower;
}

@end
