//
//  BackgroundManager.m
//  Dora
//
//  Created by Jason Chang on 8/9/12.
//
//

#import "BackgroundManager.h"
#import "GameTypes.h"

@implementation BackgroundManager

@synthesize stageBoundary = _stageBoundary;
@synthesize groundHeight = _groundHeight;

- (id) initWithParentNode: (CCNode*) node
{
    if(self = [super initWithParentNode: node])
    {
        // background sprite
        [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGB565];
        _backBG = [CCSprite spriteWithFile:@"bg_back.png"];
        [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA4444];
        _frontBG = [CCSprite spriteWithFile:@"bg_front.png"];
        [_parent addChild: _backBG z: spriteDepthBackBG];
        [_parent addChild: _frontBG z: spriteDepthFrontBG];
        _backBG.anchorPoint = CGPointZero;
        _frontBG.anchorPoint = CGPointZero;
        [self updateBGPosition];
        
        _stageBoundary = CGRectMake(0, 0, 960, 320);
        _groundHeight = 30;
        // TODO
        // make backbg move at to fill this length
        // make front bg tile to 1:1 ratio with this length
    }
    return self;
}

- (void) updateBGPosition
{
    //float size = _backBG.boundingBox.size.width/2;
    _backBG.position = CGPointZero;
    _frontBG.position = _backBG.position;
}

@end
