//
//  PhysicsManager.h
//  Dora
//
//  Created by  on 12/28/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Box2D.h"
#import "GameContactListener.h"

@class GameObject;

@interface PhysicsManager : NSObject
{
}

@property (nonatomic, readonly) float inversePTM;

- (void) update: (ccTime) dt;
- (void) reset;
- (void) draw;


@end
