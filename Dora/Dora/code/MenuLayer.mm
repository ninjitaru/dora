//
//  MenuLayer.m
//  Dora
//
//  Created by  on 4/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MenuLayer.h"
#import "GameLayer.h"
#import "SimpleAudioEngine.h"

@implementation MenuLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	MenuLayer *layer = [MenuLayer node];
	[scene addChild: layer];
	return scene;
}

- (id) init
{
    if((self = [super init]))
    {
        [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGB565];
        
        CGSize size = [CCDirector sharedDirector].winSize;
        
        CCSprite* menuBG = [CCSprite spriteWithFile: @"bg_menu.png"];
        menuBG.position = ccp(0,0);
        menuBG.anchorPoint = ccp(0,0);
        [self addChild: menuBG];
        
        [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA4444];
        
        _startButton = [CCSprite spriteWithFile: @"button_start.png"];
        _startButton.position = ccp(size.width/2, size.height*0.3);
        [self addChild: _startButton];
        
        self.isTouchEnabled = YES;
        
        _fadeToGray = [[CCTintTo actionWithDuration: 0.5f red: 50 green: 50 blue: 50] retain];
        _fadeToWhite = [[CCTintTo actionWithDuration: 0.5f red: 255 green: 255 blue: 255] retain];
}
    return self;
}

- (void) dealloc
{
    [_startButton stopAllActions];
    [_fadeToGray release];
    [_fadeToWhite release];
    [super dealloc];
}

- (void) onEnter
{
    [super onEnter];
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
}

- (void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch* touch in touches)
    {
        CGPoint pos = [self convertTouchToNodeSpace: touch];
        if(CGRectContainsPoint(_startButton.boundingBox, pos))
        {
            _runningFadeToGray = YES;
            [_startButton stopAllActions];
            [_startButton runAction: _fadeToGray];
        }
    }
}

- (void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch* touch in touches)
    {
        CGPoint pos = [self convertTouchToNodeSpace: touch];
        if(CGRectContainsPoint(_startButton.boundingBox, pos))
        {
            if(!_runningFadeToGray)
            {
                _runningFadeToGray = YES;
                [_startButton stopAllActions];
                [_startButton runAction: _fadeToGray];
            }
        }
        else
        {
            if(_runningFadeToGray)
            {
                _runningFadeToGray = NO;
                [_startButton stopAllActions];
                [_startButton runAction: _fadeToWhite];
            }
        }
    }
}

- (void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch* touch in touches)
    {
        CGPoint pos = [self convertTouchToNodeSpace: touch];
        if(CGRectContainsPoint(_startButton.boundingBox, pos))
        {
            _runningFadeToGray = NO;
            [_startButton stopAllActions];
            [_startButton runAction: _fadeToWhite];
            CCLabelTTF* text = [CCLabelTTF labelWithString: @"LOADING" fontName: @"Marker Felt" fontSize: 64];
            text.color = ccWHITE;
            CGSize size = [CCDirector sharedDirector].winSize;
            text.position = ccp(size.width/2, size.height*0.8f);
            [self addChild: text];
            [self runAction: [CCSequence actions:[CCDelayTime actionWithDuration: 0.5f], [CCCallFunc actionWithTarget:self selector: @selector(startGameScene)], nil]];
            
            return;
        }
    }
}

- (void) ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self ccTouchesEnded: touches withEvent: event];
}

- (void) startGameScene
{
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration: 0.5f scene: [GameLayer scene] withColor: ccWHITE]];
}

@end
