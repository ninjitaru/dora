//
//  Enemy.m
//  Dora
//
//  Created by Yuxio Tzeng on 12/4/28. Modified by Jason Chang 12/8/12
//  Copyright (c) 2012年 Qiran Co., Ltd. All rights reserved.
//

#include <stdlib.h>
#import "Enemy.h"

static NSArray* npcAWalkFrames;
static NSArray* npcBWalkFrames;
static NSArray* npcAAttackFrames;
static NSArray* npcBAttackFrames;

const float NEAR_ENOUGH_RANGE = 45.0;
#define DEFAULTSPEED 120.0f

@implementation Enemy

@synthesize state = _state;
@synthesize enemyType = _enemyType;
@synthesize recentlyDead = _recentlyDead;

@synthesize power = _power;

- (void) setState:(enumEnemyState)state
{
    if(_state == state)
        return;
    
    switch(state)
    {
        case stateNpcNone:
            _recentlyDead = NO;
            [self.sprite stopAllActions];
            self.sprite.visible = NO;
            self.sprite.opacity = 255;
            _target = nil;
            _hp = 1;
            self.isValid = NO;
            _delay = 0;
            break;
        case stateNpcWalk:
            _delay = 0;
            self.sprite.visible = YES;
            self.sprite.opacity = 255;
            self.isValid = YES;
            [self.sprite stopAllActions];
            switch(self.enemyType)
            {
                case eTypeA:
                    [self.sprite runAction: [_animationDict objectForKey:@"NpcAWalk"]];
                    _speedRatio = 0.3f + CCRANDOM_0_1() * 0.1f;
                    break;
                case eTypeB:
                    [self.sprite runAction: [_animationDict objectForKey:@"NpcBWalk"]];
                    _speedRatio = 0.5f + CCRANDOM_0_1() * 0.1f;
                    break;
            }
            break;
        case stateNpcAttack:
            [self.sprite stopAllActions];
            switch(self.enemyType)
            {
                case eTypeA:
                    [self.sprite runAction: [_animationDict objectForKey:@"NpcAAttack"]];
                    break;
                case eTypeB:
                    [self.sprite runAction: [_animationDict objectForKey:@"NpcBAttack"]];
                    break;
            }
            break;
        case stateNpcDead:
            _recentlyDead = YES;
            self.isValid = NO;
            _target = nil;
            _hp = 1;
            [self.sprite stopAllActions];
            [self.sprite runAction: [CCSequence actions:[CCFadeOut actionWithDuration: 0.5f],[CCCallBlock actionWithBlock: ^{ self.state = stateNpcNone; }],nil]];
            break;
    }
    _state = state;
}

- (id) initWithParent:(CCNode*)parent withContext:(id<EnemyContextDelegate>)context;
{
    self = [super init];
    if(self)
    {
        [self initAnimation];
        _delegate = context;
        
        _power = 1;
        self.sprite = [CCSprite spriteWithSpriteFrameName: @"Cnpc_01.png"];
        self.sprite.anchorPoint = ccp(0.5f, 0);
        self.sprite.visible = NO;
        self.state = stateNpcNone;
        _enemyType = eTypeA;
        [parent addChild: self.sprite z: spriteDepthEnemyBack];
    }
    
    return self;
}

- (void) dealloc
{
    [_animationDict release];
    [super dealloc];
}

- (void) spawnAs: (enumEnemyType) type
{
    self.state = stateNpcNone;
    _enemyType = type;
    
    switch (self.enemyType)
    {
        case eTypeA:
            _dmgDistance = NEAR_ENOUGH_RANGE;
            _power = 10;
            break;
        case eTypeB:
            _dmgDistance = NEAR_ENOUGH_RANGE * 3;
            _power = 1;
            break;
    }
    
    if(CCRANDOM_0_1() > 0.5)
        [self.sprite.parent reorderChild: self.sprite z: spriteDepthEnemyBack];
    else
        [self.sprite.parent reorderChild: self.sprite z: spriteDepthEnemyFront];
    self.state = stateNpcWalk;
    
    

}

- (void) dmgTarget
{
    [_target receiveHit: _power];
}

- (void)walkToTarget: (ccTime)dt
{
    _delay = 0;
    self.state = stateNpcWalk;
    int directionModifier = 0;
    
    if (self.sprite.position.x < (_target.sprite.position.x - _dmgDistance))
    {
        self.sprite.flipX = NO;
        directionModifier = 1;
    }
    else if (self.sprite.position.x > (_target.sprite.position.x + _dmgDistance))
    {
        // at right side, walk to left
        directionModifier = -1;
        self.sprite.flipX = YES;
    }
    
    CGPoint pos = self.sprite.position;
    self.sprite.position = ccp(pos.x + DEFAULTSPEED * _speedRatio * dt * directionModifier, pos.y);
}

#define NPCATTACKDELAY 0.6f // this also helps reduce the image swapping between walk/attack

-(void) update: (ccTime) dt
{
    if(self.state == stateNpcNone || self.state == stateNpcDead)
        return;
    
    if (_target)
    {
        if ([_target isValid])
        {
            if ([_target distanceTo:self] < _dmgDistance)
            {
                if(_delay >= NPCATTACKDELAY)
                {
                    self.state = stateNpcAttack;
                }
                else
                {
                    _delay++;
                }
            }
            else
            {
                [self walkToTarget:dt];
            }
        }
        else
        {
            _target = [_delegate findNewTarget: self.sprite.position.x];
        }
    }
    else
    {
        _target = [_delegate findNewTarget: self.sprite.position.x];
    }

    
}

- (void)initAnimation
{
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];

    // NPC A Walk

    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:npcAWalkFrames delay:0.1f];
    CCRepeatForever *repeat = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation: animation]];
    [dict setObject:repeat forKey: @"NpcAWalk"];

    // NPC B Walk

    animation = [CCAnimation animationWithSpriteFrames:npcBWalkFrames delay:0.1f];
    repeat = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:animation]];
    [dict setObject:repeat forKey: @"NpcBWalk"];
    
    // NPC A Attack
    
    animation = [CCAnimation animationWithSpriteFrames:npcAAttackFrames delay:0.1f];
    id action = [CCSequence actions: [CCAnimate actionWithAnimation: animation],[CCCallBlock actionWithBlock: ^{ [self dmgTarget]; }], nil];
    repeat = [CCRepeatForever actionWithAction: action];
    [dict setObject:repeat forKey: @"NpcAAttack"];    
    
    // NPC B Attack
    
    animation = [CCAnimation animationWithSpriteFrames:npcBAttackFrames delay:0.2f];
    
    action = [CCSequence actions: [CCAnimate actionWithAnimation: animation],[CCCallBlock actionWithBlock: ^{ [self dmgTarget]; }], nil];
    repeat = [CCRepeatForever actionWithAction: action];
    [dict setObject:repeat forKey: @"NpcBAttack"];
    
    _animationDict = [dict retain];
}

- (void) receiveHit:(int)dmgPower
{
    _hp -= dmgPower;
    if(_hp <= 0)
    {
        self.state = stateNpcDead;
    }
}

// called once with class
+ (void)initialize
{
    CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    
    npcAWalkFrames = [@[
                      [cache spriteFrameByName: @"Cnpc_01.png"],
                      [cache spriteFrameByName: @"Cnpc_02.png"],
                      [cache spriteFrameByName: @"Cnpc_03.png"],
                      [cache spriteFrameByName: @"Cnpc_04.png"],
                      [cache spriteFrameByName: @"Cnpc_05.png"],
                      [cache spriteFrameByName: @"Cnpc_06.png"]] retain];
    
    npcBWalkFrames = [@[
                      [cache spriteFrameByName: @"Rnpc_01.png"],
                      [cache spriteFrameByName: @"Rnpc_02.png"],
                      [cache spriteFrameByName: @"Rnpc_03.png"],
                      [cache spriteFrameByName: @"Rnpc_04.png"],
                      [cache spriteFrameByName: @"Rnpc_05.png"],
                      [cache spriteFrameByName: @"Rnpc_06.png"]] retain];
    
    npcAAttackFrames = [@[
                        [cache spriteFrameByName:@"CnpcA_01.png"],
                        [cache spriteFrameByName:@"CnpcA_02.png"],
                        [cache spriteFrameByName:@"CnpcA_03.png"],
                        [cache spriteFrameByName:@"CnpcA_04.png"]] retain];
    
    npcBAttackFrames = [@[
                        [cache spriteFrameByName:@"RnpcA_01.png"],
                        [cache spriteFrameByName:@"RnpcA_02.png"],
                        [cache spriteFrameByName:@"RnpcA_03.png"],
                        [cache spriteFrameByName:@"RnpcA_04.png"]] retain];
}

@end