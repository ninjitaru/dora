//
//  UILayer.h
//  Dora
//
//  Created by  on 4/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "CCLayer.h"
#import "TouchTracker.h"
#import "GameTypes.h"

typedef enum
{
    ModePlay,
    ModePause,
    ModeGameOver,
} enumUIMode;

@interface UILayer : CCLayer <ControlDelegate>
{
    TouchTracker* _touchTracker;
    CCSpriteFrameCache* _cache;
    CCSpriteBatchNode* _interfaceBatchNode;
    
    // in game hud display
    CCSprite* _hpSprite;
    
    // in game control
    CCSprite* _pad;
    
    /* attack1, attack2, pause */
    CCSprite** _buttons;
    CGRect* _detectionRects;
    
    // pause image
    CCLabelTTF* _restart;
    CCLabelTTF* _score;
    CCSprite* _overlayBG;
    
    // gameover image
    CCLabelTTF* _npcCount;
    CCLabelTTF* _survivedTime;
    
    enumUIMode _uiMode;
    
    enumButtonPhase* _buttonState; // pause, melee, range, grab, left, right, retry
    int* _buttonTouchIDs;
    CGRect _directionDetectionRegion;
    CGRect _buttonDetectionRegion;
}

@property (nonatomic, assign) int killedCount;
@property (nonatomic, assign) float time;
@property (nonatomic, assign) int waveCount;
//@property (nonatomic, assign) BOOL gameOver;
@property (nonatomic, assign) float hpRatio;

- (BOOL) isButtonDown: (enumUIButton) type;
- (BOOL) isButtonUp: (enumUIButton) type;

- (void) showGameOver;
- (void) reset;

@end
