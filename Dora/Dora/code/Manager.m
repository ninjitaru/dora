//
//  Manager.m
//  Dora
//
//  Created by Jason Chang on 8/9/12.
//
//

#import "Manager.h"

@implementation Manager

- (id) initWithParentNode: (CCNode*) node
{
    if(self = [super init])
    {
        _parent = node;
    }
    return self;
}

- (void) reset
{
    
}

- (void) update: (ccTime) dt
{
    
}

@end
