//
//  Entity.h
//  Dora
//
//  Created by Yuxio Tzeng on 12/4/29. Modified by Jason Chang 12/8/12
//  Copyright (c) 2012年 Qiran Co., Ltd. All rights reserved.
//

#import "cocos2d.h"

@interface Entity : NSObject

@property (assign, nonatomic) BOOL isValid;
@property (assign, nonatomic) CCSprite* sprite;

- (float)distanceTo:(Entity *)target;
- (void) receiveHit: (int) dmgPower;

@end
