//
//  Hero.m
//  Dora
//
//  Created by Jason Chang on 8/9/12.
//
//

#import "Hero.h"
#import "SimpleAudioEngine.h"

typedef enum
{
    tagNormalAction = 1,
    tagHurtAction,
} heroActionTag;

@implementation Hero

@synthesize state = _state;
@synthesize direction = _direction;

@synthesize hp = _hp;
@synthesize defense = _defense;
@synthesize power = _power;

- (id)initWithParent: (CCNode*) parent
{
    if((self = [super init]))
    {
        _cache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [self initAnimation: parent];
    }
    return self;
}

- (void) dealloc
{
    [_animationDictionary release];
    [super dealloc];
}
//
//- (float)getPosition
//{
//    return self.sprite.position.x;
//}

- (BOOL)isValid
{
    return YES;
}

- (void) receiveHit: (int) dmgPower;
{
    if(dmgPower > _defense)
    {
        _hp -= (dmgPower - _defense);
        if(_hp < 0)
            _hp = 0;
        
        // TODO a better indication of hero receive dmg
        //[self.sprite stopActionByTag: tagHurtAction];
        //self.sprite.visible = YES;
        //[self.sprite runAction: [_animationDictionary valueForKey: @"HeroHurt"]];
    }
}

- (void) setState:(enumHeroState)state
{
    if(_state == state)
        return;
    
    switch(state)
    {
        case stateHeroNone:
            [self.sprite stopAllActions];
            [self.sprite setDisplayFrame: [_cache spriteFrameByName: @"DoraStand_01.png"]];
            _state = stateHeroNone;
            break;
        case stateHeroDead:
            [[SimpleAudioEngine sharedEngine] playEffect: @"dora_dead.wav"];
            [self.sprite stopActionByTag: tagNormalAction];
            [self.sprite setDisplayFrame: [_cache spriteFrameByName: @"DoraDie.png"]];
            _state = stateHeroDead;
            break;
        case stateHeroIdle:
            if(_state != stateHeroMelee && _state != stateHeroRange)
            {
                [self.sprite stopActionByTag: tagNormalAction];
                [self.sprite runAction: [_animationDictionary valueForKey: @"HeroIdle"]];
                _state = stateHeroIdle;
            }
            break;
        case stateHeroMelee:
            self.carryMeleeDmg = YES;
            [[SimpleAudioEngine sharedEngine] playEffect: @"dora_melee.mp3"];
            [self.sprite stopActionByTag: tagNormalAction];
            [self.sprite runAction: [_animationDictionary valueForKey: @"HeroMeleeAttack1"]];
            _state = stateHeroMelee;
            break;
        case stateHeroMeleeStop:
            if(_state == stateHeroMelee)
            {
                [self.sprite stopActionByTag: tagNormalAction];
                _state = stateHeroMeleeStop;
            }
            break;
        case stateHeroRange:
            [self.sprite stopActionByTag: tagNormalAction];
            [self.sprite runAction: [_animationDictionary valueForKey: @"HeroRangeAttack"]];
            _state = stateHeroRange;
            break;
        case stateHeroRangeStop:
            if(_state == stateHeroRange)
            {
                [self.sprite stopActionByTag: tagNormalAction];
                _state = stateHeroRangeStop;
            }
            break;
        case stateHeroWalk:
            if(_state != stateHeroMelee && _state != stateHeroRange)
            {
                [self.sprite stopActionByTag: tagNormalAction];
                [self.sprite runAction: [_animationDictionary valueForKey: @"HeroWalk"]];
                _state = stateHeroWalk;
            }
            break;
    }
}

- (void) setDirection:(enumMoveDirection)direction
{
    if(_direction == direction)
        return;
    _direction = direction;
    if(_direction == kDirectionLeft)
    {
        if(self.sprite.scaleX > 0)
            self.sprite.scaleX *= -1;
    }
    else
    {
        if(self.sprite.scaleX < 0)
            self.sprite.scaleX *= -1;
    }
}

- (void) initAnimation: (CCNode*) parent;
{
    if(self.sprite == nil)
    {
        // hero sprite
        self.sprite = [CCSprite spriteWithSpriteFrameName:@"DoraStand_01.png"];
        [parent addChild: self.sprite z: spriteDepthHero];
        self.sprite.anchorPoint = ccp(0.5f, 0);
    
        NSMutableDictionary* dictionary = [NSMutableDictionary dictionary];
        
        NSArray* array = @[
                          [_cache spriteFrameByName: @"DoraWalk_01.png"],
                          [_cache spriteFrameByName: @"DoraWalk_02.png"],
                          [_cache spriteFrameByName: @"DoraWalk_03.png"],
                          [_cache spriteFrameByName: @"DoraWalk_04.png"],
                          [_cache spriteFrameByName: @"DoraWalk_05.png"],
                          [_cache spriteFrameByName: @"DoraWalk_06.png"]
        ];
        
        CCAnimation *moveAnimation = [CCAnimation animationWithSpriteFrames: array delay:0.1f];
        CCAction* move = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation: moveAnimation]];
        move.tag = tagNormalAction;
        [dictionary setObject: move forKey: @"HeroWalk"];
        
        array = @[
                 [_cache spriteFrameByName: @"DoraAttack_01.png"],
                 [_cache spriteFrameByName: @"DoraAttack_02.png"],
                 [_cache spriteFrameByName: @"DoraAttack_03.png"],
                 [_cache spriteFrameByName: @"DoraAttack_04.png"],
                 [_cache spriteFrameByName: @"DoraAttack_05.png"]
                 ];
        
        CCAction* _shortAttackAnimation = [CCSequence actions:[CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames: array delay: 0.1f]], [CCCallBlock actionWithBlock: ^{ self.state = stateHeroMeleeStop; }], nil];
        _shortAttackAnimation.tag = tagNormalAction;
        [dictionary setObject: _shortAttackAnimation forKey: @"HeroMeleeAttack1"];
        
        array = @[
                 [_cache spriteFrameByName: @"DoraRange_01.png"],
                 [_cache spriteFrameByName: @"DoraRange_02.png"],
                 [_cache spriteFrameByName: @"DoraRange_03.png"],
                 ];
        
        CCAction* _longAttackAnimation = [CCSequence actions:[CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames: array delay: 0.1f]], [CCCallBlock actionWithBlock: ^{ self.state = stateHeroRangeStop; }], nil];
        _longAttackAnimation.tag = tagNormalAction;
        [dictionary setObject: _longAttackAnimation forKey: @"HeroRangeAttack"];
        
        array = @[
                 [_cache spriteFrameByName: @"DoraStand_01.png"],
                 [_cache spriteFrameByName: @"DoraStand_02.png"],
                 [_cache spriteFrameByName: @"DoraStand_03.png"],
                 ];
        
        CCAction* idle = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames: array delay: 0.2f]]];
        idle.tag = tagNormalAction;
        [dictionary setObject: idle forKey: @"HeroIdle"];
        
        CCAction* hurt = [CCBlink actionWithDuration: 1.0f blinks: 2];
        hurt.tag = tagHurtAction;
        [dictionary setObject: hurt forKey: @"HeroHurt"];
        
        _animationDictionary = [[NSDictionary dictionaryWithDictionary: dictionary] retain];
    }
    
    self.state = stateHeroNone;
}

@end
