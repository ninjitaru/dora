//
//  UILayer.m
//  Dora
//
//  Created by  on 4/28/12. Modified by Jason Chang 12/8/12
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UILayer.h"
#import "GameLayer.h"

@implementation UILayer

//@synthesize gameDelegate = _gameDelegate;
@synthesize hpRatio = _hpRatio;
//@synthesize gameOver = _gameOver;
@synthesize waveCount = _waveCount;
@synthesize killedCount = _killedCount;
@synthesize time = _time;

- (void) setHpRatio:(float)hpRatio
{
    if(_hpRatio == hpRatio)
        return;
    _hpRatio = hpRatio;
    if(_hpRatio < 0)
        _hpRatio = 0;
    if(_hpRatio > 1)
        _hpRatio = 1;
    _hpSprite.scaleX = _hpRatio;
}

- (id) init
{
    if((self = [super init]))
    {
        _touchTracker = [[TouchTracker alloc] init];
        self.isTouchEnabled = YES;

        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"interface.plist"];
        _cache = [CCSpriteFrameCache sharedSpriteFrameCache];
        _interfaceBatchNode = [[CCSpriteBatchNode alloc] initWithFile: @"interface.png" capacity: 10];
        [self addChild: _interfaceBatchNode z: 1];
        [_interfaceBatchNode release];
        
        [self setupHUD];
        [self setupControl];
        [self setupGameOver];
        _uiMode = ModePlay;
    }
    return self;
}

- (void) setupHUD
{
    CGSize size = [CCDirector sharedDirector].winSize;
    
    _overlayBG = [CCSprite spriteWithSpriteFrameName: @"dot.png"];
    _overlayBG.position = ccp(0,0);
    _overlayBG.anchorPoint = ccp(0,0);
    _overlayBG.scaleX = 480;
    _overlayBG.scaleY = 320;
    _overlayBG.color = ccRED;
    _overlayBG.opacity = 50;
    _overlayBG.visible = NO;
    [_interfaceBatchNode addChild: _overlayBG];
    
    _hpSprite = [CCSprite spriteWithSpriteFrameName: @"image_blood.png"];
    _hpSprite.anchorPoint = ccp(0.5f,1);
    _hpSprite.position = ccp(size.width/2, size.height * 0.98f);
    [_interfaceBatchNode addChild: _hpSprite];
    
    self.hpRatio = 1;
}

#define BUTTONCOUNT 7

- (void) setupControl
{
    _buttons = (CCSprite**)malloc(sizeof(CCSprite*)*BUTTONCOUNT);
    _detectionRects = (CGRect*)malloc(sizeof(CGRect)*BUTTONCOUNT);
    
    CGSize size = [CCDirector sharedDirector].winSize;
    
    _buttons[0] = [CCSprite spriteWithSpriteFrameName: @"button_pause.png"];
    _buttons[0].anchorPoint = ccp(1,1);
    _buttons[0].scale = 0.5f;
    _buttons[0].position = ccp(size.width - 2, size.height * 0.98f);
    
    _buttons[1] = [CCSprite spriteWithSpriteFrameName: @"button_melee_normal.png"];
    _buttons[1].anchorPoint = ccp(0,0);
    _buttons[1].position = ccp(size.width * 0.7f, 2);
    
    _buttons[2] = [CCSprite spriteWithSpriteFrameName: @"button_range_normal.png"];
    _buttons[2].anchorPoint = ccp(0,0);
    _buttons[2].position = ccp(size.width* 0.8f, 62);
    
    _buttons[3] = [CCSprite spriteWithSpriteFrameName: @"button_grab_normal.png"];
    _buttons[3].anchorPoint = ccp(0,0);
    _buttons[3].position = ccp(size.width * 0.85f, 2);
    
    _buttons[4] = [CCSprite spriteWithSpriteFrameName: @"button_left.png"];
    _buttons[4].anchorPoint = ccp(0,0);
    _buttons[4].position = ccp(size.width * 0.05f, 2);
    
    _buttons[5] = [CCSprite spriteWithSpriteFrameName: @"button_right.png"];
    _buttons[5].anchorPoint = ccp(0,0);
    _buttons[5].position = ccp(size.width * 0.2f, 2);
    
    _buttons[6] = [CCSprite spriteWithSpriteFrameName: @"button_continue.png"];
    _buttons[6].anchorPoint = ccp(0.5f, 1);
    _buttons[6].position = ccp(size.width/2, size.height*0.95f);
    _buttons[6].visible = NO;
    
    _directionDetectionRegion = CGRectUnion(_buttons[4].boundingBox, _buttons[5].boundingBox);
    _buttonDetectionRegion = CGRectUnion(CGRectUnion(_buttons[1].boundingBox, _buttons[2].boundingBox), _buttons[3].boundingBox);
    
    for(int i = 0; i < BUTTONCOUNT; i++)
    {
        [_interfaceBatchNode addChild: _buttons[i]];
        _detectionRects[i] = _buttons[i].boundingBox;
    }
    
    _buttonTouchIDs = (int*)malloc(sizeof(int)*BUTTONCOUNT);
    _buttonState = (enumButtonPhase*)malloc(sizeof(enumButtonPhase)*BUTTONCOUNT);
    for(int i = 0; i < BUTTONCOUNT; i++)
    {
        _buttonState[i] = phaseUp;
        _buttonTouchIDs[i] = -1;
    }
}

- (void) setupPause
{
    
}

- (void) setupGameOver
{
    CGSize size = [CCDirector sharedDirector].winSize;
    
    _score = [CCLabelTTF labelWithString: @"You survived 1 wave" fontName: @"Marker Felt" fontSize: 32];
    _score.visible = NO;
    _score.position = ccp(size.width/2, size.height*0.6f);
    
    _npcCount = [CCLabelTTF labelWithString: @"You killed 0 NPC" fontName: @"Marker Felt" fontSize: 26];
    _survivedTime = [CCLabelTTF labelWithString: @"You survived 0 second" fontName: @"Marker Felt" fontSize: 26];
    _npcCount.position = ccp(size.width/2, size.height*0.5f);
    _survivedTime.position = ccp(size.width/2, size.height*0.4f);
    _npcCount.visible = NO;
    _survivedTime.visible = NO;
    
    [self addChild: _score z: 2];
    [self addChild: _npcCount z: 2];
    [self addChild: _survivedTime z: 2];
}

- (void) showGameOver
{
    _score.string = [NSString stringWithFormat: @"You survived %i waves", self.waveCount];
    _npcCount.string = [NSString stringWithFormat: @"Killed %i NPC", self.killedCount];
    _survivedTime.string = [NSString stringWithFormat: @"Survived %2.1f second", self.time];
    _uiMode = ModeGameOver;
    
    _buttons[6].visible = YES;
    _buttons[6].opacity = 255;
    [_buttons[6] stopAllActions];
    [_buttons[6] runAction: [CCRepeatForever actionWithAction: [CCSequence actions: [CCFadeTo actionWithDuration: 1.0f opacity: 150], [CCFadeTo actionWithDuration: 1.0f opacity: 255], nil]]];
    _score.visible = YES;
    _overlayBG.visible = YES;
    _npcCount.visible = YES;
    _survivedTime.visible = YES;
}

- (BOOL) isButtonDown: (enumUIButton) type
{
    return _buttonState[type] == phaseDown;
}

- (BOOL) isButtonUp: (enumUIButton) type
{
    return _buttonState[type] == phaseUp;
}

- (void) setButton: (int) index withPhase: (enumButtonPhase) phase
{
    _buttonState[index] = phase;
    if(phase == phaseDown)
    {
        switch(index)
        {
            case 0:
                [_buttons[index] stopAllActions];
                [_buttons[index] runAction: [CCScaleTo actionWithDuration: 0.1f scale: 0.45f]];
                break;
            case 4:
            case 5:
            case 6:
                [_buttons[index] stopAllActions];
                [_buttons[index] runAction: [CCScaleTo actionWithDuration: 0.1f scale: 0.9f]];
                break;
            case 1:
                [_buttons[index] setDisplayFrame: [_cache spriteFrameByName: @"button_melee_down.png"]];
                break;
            case 2:
                [_buttons[index] setDisplayFrame: [_cache spriteFrameByName: @"button_range_down.png"]];
                break;
            case 3:
                [_buttons[index] setDisplayFrame: [_cache spriteFrameByName: @"button_grab_down.png"]];
                break;
        }
    }
    else
    {
        switch(index)
        {
            case 0:
                [_buttons[index] stopAllActions];
                [_buttons[index] runAction: [CCScaleTo actionWithDuration: 0.1f scale: 0.5f]];
                break;
            case 4:
            case 5:
            case 6:
                [_buttons[index] stopAllActions];
                [_buttons[index] runAction: [CCScaleTo actionWithDuration: 0.1f scale: 1.0f]];
                break;
            case 1:
                [_buttons[index] setDisplayFrame: [_cache spriteFrameByName: @"button_melee_normal.png"]];
                break;
            case 2:
                [_buttons[index] setDisplayFrame: [_cache spriteFrameByName: @"button_range_normal.png"]];
                break;
            case 3:
                [_buttons[index] setDisplayFrame: [_cache spriteFrameByName: @"button_grab_normal.png"]];
                break;
        }
    }
}

- (BOOL) setButtonPhase: (CGPoint) pos withTouchID: (int) trackID
{
    BOOL releaseTrackID = YES;
    if(CGRectContainsPoint(_directionDetectionRegion, pos))
    {
        // handle direction button
        if(_buttonTouchIDs[4] == -1 && CGRectContainsPoint(_detectionRects[4], pos))
        {
            _buttonTouchIDs[4] = trackID;
            [self setButton: 4 withPhase: phaseDown];
            releaseTrackID = NO;
        }
        else if(_buttonTouchIDs[5] == -1 && CGRectContainsPoint(_detectionRects[5], pos))
        {
            _buttonTouchIDs[5] = trackID;
            [self setButton: 5 withPhase: phaseDown];
            releaseTrackID = NO;
        }
    }
    else if(CGRectContainsPoint(_buttonDetectionRegion, pos))
    {
        // handle skill button
        if(_buttonTouchIDs[1] == -1 && CGRectContainsPoint(_detectionRects[1], pos))
        {
            _buttonTouchIDs[1] = trackID;
            [self setButton: 1 withPhase: phaseDown];
            releaseTrackID = NO;
        }
        else if(_buttonTouchIDs[2] == -1 && CGRectContainsPoint(_detectionRects[2], pos))
        {
            _buttonTouchIDs[2] = trackID;
            [self setButton: 2 withPhase: phaseDown];
            releaseTrackID = NO;
        }
        else if(_buttonTouchIDs[3] == -1 && CGRectContainsPoint(_detectionRects[3], pos))
        {
            _buttonTouchIDs[3] = trackID;
            [self setButton: 3 withPhase: phaseDown];
            releaseTrackID = NO;
        }
    }
    else
    {
        if(_buttonTouchIDs[0] == -1 && CGRectContainsPoint(_detectionRects[0], pos))
        {
            _buttonTouchIDs[0] = trackID;
            [self setButton: 0 withPhase: phaseDown];
            releaseTrackID = NO;
        }
    }
    
    return !releaseTrackID;
}

- (void) setButtonPhaseBegin: (UITouch*) touch
{
    CGPoint pos = [self convertTouchToNodeSpace: touch];
    int trackID = [_touchTracker trackTouch: touch];
    if(trackID == -1)
        return;
    
    if(![self setButtonPhase: pos withTouchID: trackID])
        [_touchTracker releaseTouchByID: trackID];
}

- (void) setButtonPhaseMove: (UITouch*) touch
{
    CGPoint pos = [self convertTouchToNodeSpace: touch];
    int trackID = [_touchTracker getTouchID: touch];
    BOOL findNewPosition = YES;
    if(trackID != -1)
    {
        for(int i = 0; i < BUTTONCOUNT; i++)
        {
            if(_buttonTouchIDs[i] == trackID)
            {
                findNewPosition = NO;
                if(!CGRectContainsPoint(_detectionRects[i], pos))
                {
                    findNewPosition = YES;
                    _buttonTouchIDs[i] = -1;
                    [self setButton: i withPhase: phaseUp];
                }
            }
        }
    }
    
    if(findNewPosition)
    {
        if(trackID == -1)
        {
            trackID = [_touchTracker trackTouch: touch];
        }
        if(trackID != -1)
        {
            [self setButtonPhase: pos withTouchID: trackID];
        }
    }
}

- (void) setButtonPhaseEnd: (UITouch*) touch
{
    int trackID = [_touchTracker getTouchID: touch];
    if(trackID == -1)
    {
        return;
    }
    
    for(int i = 0; i < BUTTONCOUNT; i++)
    {
        if(_buttonTouchIDs[i] == trackID)
        {
            _buttonTouchIDs[i] = -1;
            [self setButton: i withPhase: phaseUp];
        }
    }
    [_touchTracker releaseTouchByID: trackID];
}

- (void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch* touch in touches)
    {
        CGPoint pos = [self convertTouchToNodeSpace: touch];
        switch(_uiMode)
        {
            case ModePlay:
                [self setButtonPhaseBegin: touch];
                break;
            case ModeGameOver:
                if(CGRectContainsPoint(_buttons[6].boundingBox, pos))
                {
                    [self setButton:6 withPhase: phaseDown];
                }
                break;
            case ModePause:
                break;
        }
    }
}

- (void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch* touch in touches)
    {
        CGPoint pos = [self convertTouchToNodeSpace: touch];
        switch(_uiMode)
        {
            case ModePlay:
                [self setButtonPhaseMove: touch];
                break;
            case ModeGameOver:
                if(CGRectContainsPoint(_buttons[6].boundingBox, pos))
                {
                    if(_buttonState[6] == phaseUp)
                    {
                        [self setButton:6 withPhase: phaseDown];
                    }
                }
                else
                {
                    if(_buttonState[6] == phaseDown)
                    {
                        [self setButton:6 withPhase: phaseUp];
                    }
                }
                break;
            case ModePause:
                break;
        }
    }
}

- (void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch* touch in touches)
    {
        CGPoint pos = [self convertTouchToNodeSpace: touch];
        switch(_uiMode)
        {
            case ModePlay:
                [self setButtonPhaseEnd: touch];
                break;
            case ModeGameOver:
                if(CGRectContainsPoint(_buttons[6].boundingBox, pos))
                {
                    [self setButton:6 withPhase: phaseUp];
                }
                break;
            case ModePause:
                break;
        }
    }
}

- (void) ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self ccTouchesEnded: touches withEvent: event];
}

- (void) reset
{
    for(int i = 0; i < BUTTONCOUNT; i++)
    {
        _buttonState[i] = phaseUp;
        _buttonTouchIDs[i] = -1;
    }
    
    _uiMode = ModePlay;
    
    _score.visible = NO;
    _restart.visible = NO;
    _overlayBG.visible = NO;
    _npcCount.visible = NO;
    _survivedTime.visible = NO;
    
    [_buttons[6] stopAllActions];
    _buttons[6].opacity = 255;
    _buttons[6].visible = NO;
}

- (void) dealloc
{
    [_touchTracker release];
    if(_buttonState) free(_buttonState);
    if(_detectionRects) free(_detectionRects);
    if(_buttons) free(_buttons);
    
    [super dealloc];
}

@end
