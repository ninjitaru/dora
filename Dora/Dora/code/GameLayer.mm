//
//  GameLayer.m
//  Dora
//
//  Created by  on 4/28/12. Modified by Jason Chang 12/8/12
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameLayer.h"
#import "UILayer.h"
#import "SimpleAudioEngine.h"

#pragma mark - GameLayer

@interface GameLayer (PM)

- (void) reset;
- (void) addEnermy: (ccTime) dt;
- (void) showIdle;

@end

@implementation GameLayer

@synthesize uiDelegate = _uiDelegate;

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	GameLayer *layer = [GameLayer node];
    UILayer* uilayer = [UILayer node];
    layer.uiDelegate = uilayer;
    
	[scene addChild: layer];
    [scene addChild: uilayer];
	return scene;
}

#pragma mark - coco2d life cycle

- (id) init
{
    if((self = [super init]))
    {
        // add resources
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"dora.plist"];
        
        // managers

        _backgroundManager = [[BackgroundManager alloc] initWithParentNode: self];
        //_physicManager = [[PhysicsManager alloc] init];
        
        _batchNode = [CCSpriteBatchNode batchNodeWithFile:@"dora.png" capacity: 100];
        [self addChild: _batchNode z: spriteDepthGameObject];
        
        _heroManager = [[HeroManager alloc] initWithParentNode: _batchNode];
        _heroManager.stageBoundry = _backgroundManager.stageBoundary;
        _heroManager.groundPosition = _backgroundManager.groundHeight;
        [_heroManager spawnHero];
        [_heroManager spawnSideKick];
        
        _enemyManager = [[EnemyManager alloc] initWithParentNode: _batchNode];
        _enemyManager.contextDelegate = _heroManager;
        _enemyManager.stageBoundry = _backgroundManager.stageBoundary;
        _enemyManager.groundPosition = _backgroundManager.groundHeight;
        [_enemyManager prepareSpawnEnemies];
        
        [self runAction: [CCFollow actionWithTarget: _heroManager.heroSprite worldBoundary: _backgroundManager.stageBoundary]];
        
        [self scheduleUpdate];
    }
    return self;
}

- (void) onEnter
{
    [super onEnter];
    
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic: @"BackGround_01.wav"];
    [SimpleAudioEngine sharedEngine].backgroundMusicVolume = 1.0f;
    _heroManager.controlDelegate = self.uiDelegate;
}

- (void) reset
{
    [_backgroundManager reset];
    [_heroManager reset];
    [_enemyManager reset];
    //[_physicManager reset];
}

- (void) dealloc
{
    [_backgroundManager release];
    [_heroManager release];
    [_enemyManager release];
    //[_physicManager release];
    [super dealloc];
}

- (void) gameover
{
    if(!_gameOver)
    {
        [_heroManager detachAllSideKicks];
        self.uiDelegate.waveCount = _enemyManager.currentWave;
        self.uiDelegate.killedCount = _enemyManager.killCount;
        self.uiDelegate.time = _timeAccum;
        [self.uiDelegate showGameOver];
        _gameOver = YES;
        _retryClicked = NO;
        _retryDown = NO;
    }
}

- (void) restart
{
    self.uiDelegate.hpRatio = 1;
    _gameOver = NO;
    _timeAccum = 0;
    
    [self.uiDelegate reset];
    [self reset];
}


- (void) update: (ccTime) dt
{
    if(_gameOver)
    {
        if(_retryClicked)
        {
            _retryClicked = NO;
            [self restart];
        }
        if([self.uiDelegate isButtonDown: ButtonRetry] && !_retryDown)
        {
            _retryDown = YES;
        }
        if(_retryDown && [self.uiDelegate isButtonUp: ButtonRetry])
        {
            _retryDown = NO;
            _retryClicked = YES;
        }
    }
    
    [_backgroundManager update: dt];
    [_heroManager update: dt];
    [_enemyManager update: dt];
    [_heroManager tryKillEnemies: _enemyManager.enemyArray];
    //[_physicManager update: dt];
    
    self.uiDelegate.hpRatio = _heroManager.hpRatio;
    
    if(_heroManager.isHeroDead)
    {
        [self gameover];
    }
    
    _timeAccum += dt;
}

@end
