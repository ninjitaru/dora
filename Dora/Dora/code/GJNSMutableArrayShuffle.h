//
//  GJNSMutableArrayShuffle.h
//  Dora
//
//  Created by Yuxio Tzeng on 12/4/29.
//  Copyright (c) 2012年 Qiran Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

// interface
@interface NSMutableArray(Shuffling)
-(void) shuffle;
@end