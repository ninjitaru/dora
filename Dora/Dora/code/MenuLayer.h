//
//  MenuLayer.h
//  Dora
//
//  Created by  on 4/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "CCLayer.h"

@interface MenuLayer : CCLayer
{
    CCSprite* _startButton;
    id _fadeToGray;
    id _fadeToWhite;
    BOOL _runningFadeToGray;
}

+(CCScene *) scene;

@end
