//
//  EnemyManager.m
//  Dora
//
//  Created by Jason Chang on 8/9/12.
//
//

#import "EnemyManager.h"
#import "Enemy.h"
#import "GJNSMutableArrayShuffle.h"
#import "GameTypes.h"

const int MAX_NPC_COUNT = 32;
const float WAVE_WAITTIME = 5.0f;
const float OUT_SCREEN_OFFSET = 80.0;

#define WAVELIMIT 100

@implementation EnemyManager

@synthesize killCount = _killCount;
@synthesize currentWave = _currentWave;

- (NSArray*) enemyArray
{
    return _enemies;
}

- (id) initWithParentNode:(CCNode *)node
{
    if(self = [super initWithParentNode: node])
    {
        _spawnNpcTypeList = [[NSMutableArray alloc] init];
        
        _spawnInterval = 0.5f;
        _spawnTimer = _spawnInterval;

        _currentWave = 0;
        _isWaitingBetweenWaves = YES;
        _passedTimeOfWaiting = 0.0;
    }
    return self;
}

- (void) prepareSpawnEnemies
{
    NSMutableArray* array = [NSMutableArray arrayWithCapacity: MAX_NPC_COUNT];
    
    for(int i = 0; i < MAX_NPC_COUNT; i++)
    {
        Enemy* e = [[Enemy alloc] initWithParent: _parent withContext: self.contextDelegate];
        e.state = stateNpcNone;
        [array addObject: e];
    }
    
    _enemies = [array retain];
}

- (void) dealloc
{
    [_enemies release];
    [_spawnNpcTypeList release];
    [super dealloc];
}

- (void) reset
{
    _currentWave = 0;
    _killCount = 0;
    _isWaitingBetweenWaves = YES;
    _passedTimeOfWaiting = 0.0;
    _spawnTimer = _spawnInterval;
    [_spawnNpcTypeList removeAllObjects];
    
    for (Enemy* e in _enemies)
    {
        e.state = stateNpcNone;
    }
}

- (void) update:(ccTime)dt
{
    [self spawnEnemy: dt];
    
    for (Enemy* e in _enemies)
    {
        [e update:dt];
        if(e.recentlyDead)
        {
            _killCount++;
            e.recentlyDead = NO;
        }
    }
}

-(void) spawnEnemy: (ccTime) dt
{
    if (_isWaitingBetweenWaves)
    {
        _passedTimeOfWaiting += dt;
        if (_passedTimeOfWaiting > WAVE_WAITTIME)
        {
            _isWaitingBetweenWaves = NO;
            _currentWave += 1;
            [self generateSpawnNpcTypeList];
            _passedTimeOfWaiting = 0;
        }
    }
    else
    {
        if (_spawnNpcTypeList.count == 0)
        {
            _isWaitingBetweenWaves = YES;
        }
        else
        {
            if(_spawnTimer <= 0)
            {
                _spawnTimer = _spawnInterval;
                [self createEnemy];
            }
            else
            {
                _spawnTimer -= dt;
            }
        }
    }
}

- (void) createEnemy
{
    if(CCRANDOM_0_1() < 0.3f)
        return;
    for(Enemy* e in _enemies)
    {
        if(e.state == stateNpcNone)
        {
            [e spawnAs: [[_spawnNpcTypeList lastObject] intValue]];
            if(CCRANDOM_0_1() > 0.5f)
            {
                e.sprite.position = ccp(-OUT_SCREEN_OFFSET, self.groundPosition);
            }
            else
            {
                e.sprite.position = ccp(self.stageBoundry.size.width+OUT_SCREEN_OFFSET, self.groundPosition);
            }
            [_spawnNpcTypeList removeLastObject];
            return;
        }
    }
}

- (void) generateSpawnNpcTypeList
{
    if(_currentWave > WAVELIMIT)
    {
        return;
    }
    
    // TODO profile this whole method
    [_spawnNpcTypeList removeAllObjects];
    
    int _npcALeftInThisWave = _currentWave * 3 + 1 + ((_currentWave + 3) * CCRANDOM_0_1());
    int _npcBLeftInThisWave = _currentWave * 7 + 3 + ((_currentWave + 1) * CCRANDOM_0_1());
    
    for (int i = 0; i < _npcALeftInThisWave; ++i) {
        [_spawnNpcTypeList addObject: @(eTypeA)];
    }
    
    for (int i = 0; i < _npcBLeftInThisWave; ++i) {
        [_spawnNpcTypeList addObject: @(eTypeB)];
    }
    
    
    //CCLOG(@"spawn a = %i , spawn b = %i", _npcALeftInThisWave, _npcBLeftInThisWave);
    // TODO change shuffle to pure c array with random npctype id
    [_spawnNpcTypeList shuffle];
}

@end
