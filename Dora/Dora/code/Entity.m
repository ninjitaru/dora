//
//  Entity.m
//  Dora
//
//  Created by Yuxio Tzeng on 12/4/29. Modified by Jason Chang 12/8/12
//  Copyright (c) 2012年 Qiran Co., Ltd. All rights reserved.
//

#import "Entity.h"

@implementation Entity

@synthesize sprite = _sprite;

- (float)distanceTo:(Entity *)target
{
    return ABS(self.sprite.position.x - target.sprite.position.x);
}

- (void) receiveHit: (int) dmgPower
{
}

@end
