//
//  PhysicConstants.h
//  Dora
//
//  Created by  on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef FruitDino_PhysicConstants_h
#define FruitDino_PhysicConstants_h

#define PTM_RATIO 60.0f
#define INVERSE_PTM (1/PTM_RATIO)

#endif
