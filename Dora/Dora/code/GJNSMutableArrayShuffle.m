//
//  GJNSMutableArrayShuffle.m
//  Dora
//
//  Created by Yuxio Tzeng on 12/4/29.
//  Copyright (c) 2012年 Qiran Co., Ltd. All rights reserved.
//

#import "GJNSMutableArrayShuffle.h"

@implementation NSMutableArray (Shuffling)

- (void)shuffle
{
    NSUInteger count = [self count];
    for (NSUInteger i = 0; i < count; ++i) {
        // Select a random element between i and end of array to swap with.
        int nElements = count - i;
        int n = (random() % nElements) + i;
        [self exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
}

@end
